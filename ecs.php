<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use PhpCsFixer\Fixer\ControlStructure\SwitchCaseSemicolonToColonFixer;
use PhpCsFixer\Fixer\ControlStructure\TrailingCommaInMultilineFixer;
use PhpCsFixer\Fixer\ControlStructure\YodaStyleFixer;
use PhpCsFixer\Fixer\Operator\BinaryOperatorSpacesFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig $containerConfigurator): void {
    $services = $containerConfigurator->services();
    $services->set(ArraySyntaxFixer::class)
        ->call('configure', [
            [
                'syntax' => 'short',
            ],
        ]);
    $services->set(YodaStyleFixer::class)
        ->call('configure', [
            [
                'equal' => false,
                'identical' => false,
                'less_and_greater' => false,
            ],
        ]);
    $services->set(TrailingCommaInMultilineFixer::class)
        ->call('configure', [
            [
                'elements' => ['arrays'],
            ],
        ]);

    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::PATHS, [
        __DIR__ . '/app',
        __DIR__ . '/routes',
        __DIR__ . '/database',
        __DIR__ . '/config',
        __DIR__ . '/tests',
        __DIR__ . '/lang',
        __DIR__ . '/ecs.php',
    ]);

    $containerConfigurator->import(SetList::CLEAN_CODE);
    $containerConfigurator->import(SetList::PSR_12);

    $parameters->set(Option::SKIP, [
        BinaryOperatorSpacesFixer::class,
        SwitchCaseSemicolonToColonFixer::class, //conflict with Enums in PHP 8.1
    ]);

    $parameters->set(Option::PARALLEL, true);
    $parameters->set(Option::CACHE_DIRECTORY, '.ecs_cache');
};
